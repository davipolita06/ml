from __future__ import unicode_literals, print_function
from __future__ import unicode_literals

from pathlib import Path

import pandas as pd
import spacy
import copy
from spacy.util import minibatch, compounding
import re


def clean_string(mystring):
    return re.sub("[^A-Za-z\ 0-9 ]+", "", mystring)


def main(model=None, output_dir=None, n_iter=2):
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("en")  # create blank Language class
        print("Created blank 'en' model")

    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if "textcat" not in nlp.pipe_names:
        textcat = nlp.create_pipe("textcat")
        nlp.add_pipe(textcat, last=True)
    # otherwise, get it, so we can add labels to it
    else:
        textcat = nlp.get_pipe("textcat")

    # add label to text classifier
    for i in [
        "OTHERS",
        "MECHANICAL COMPONENTS",
        "MANUFACTURED PARTS",
        "FASTENERS",
        "ELECTRICAL COMPONENTS",
        "MAGNETIC WIRE",
        "NON-PRODUCTIVE",
        "ELECTRONIC COMPONENTS",
        "TOOLS",
        "NON-FERROUS",
        "FRAMES",
        "UNCLASSIFIED",
        "INSULATION",
        "BEARINGS",
        "STEEL - BARS",
        "SPARE PARTS",
        "CHEMICALS",
        "PACKAGING",
        "STEEL - ELECTRICAL",
        "STEEL - MECHANICAL",
        "SLEEVE BEARINGS",
        "CASTING SUPPLIES",
        "SOLAR ENERGY",
        "WATT",
    ]:
        textcat.add_label(i)

    df = pd.read_csv("text_emotion.csv")
    df.drop(["tweet_id", "author"], axis=1, inplace=True)
    df = df[df["sentiment"] != "empty"]

    sentiment_values = df["sentiment"].unique()
    labels_default = dict((v, 0) for v in sentiment_values)

    train_data = []
    for i, row in df.iterrows():

        label_values = copy.deepcopy(labels_default)
        label_values[row["sentiment"]] = 1

        train_data.append((str(clean_string(row["content"])), {"cats": label_values}))

    train_data = train_data[:500]

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        optimizer = nlp.begin_training()
        print("Training the model...")
        print("{:^5}\t".format("LOSS"))
        for i in range(n_iter):
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(train_data, size=compounding(4.0, 32.0, 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                # print('texts: '+str(texts))
                # print('annotations: '+str(annotations))
                nlp.update(texts, annotations, sgd=optimizer, drop=0.2, losses=losses)
            # with textcat.model.use_params(optimizer.averages):
            # evaluate on the dev data split off in load_data()

            print("{0:.3f}".format(losses["textcat"]))  # print a simple table

    # test the trained model
    test_text = "This movie sucked"
    doc = nlp(test_text)
    print(test_text, sorted(doc.cats.items(), key=lambda val: val[1], reverse=True))

    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        doc2 = nlp2(test_text)
        print(test_text, doc2.cats)


if __name__ == "__main__":
    main()